<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'order'], function () {
	//to get all orders
	Route::get('/', [OrderController::class, 'index'])->name('api.orders');
	//to get specific orders
	Route::get('/find/{id?}', [OrderController::class, 'index'])->name('api.orders.find');
	//post order
	Route::post('/post/{id?}', [OrderController::class, 'post'])->name('api.orders.post');
	//attach product with order
	Route::post('/attach_product/{id}', [OrderController::class, 'attach_product'])->name('api.orders.attach.product');
	//delete order
	Route::post('/delete/{id}', [OrderController::class, 'delete'])->name('api.orders.delete');
});

Route::group(['prefix' => 'payment'], function () {
	//to pay amonut
	Route::post('/{order_id}', [PaymentController::class, 'payment'])->name('api.payment');
	//superpay webhook for payment confirmation
	Route::post('superpay/webhook', [PaymentController::class, 'superpay_webhook'])->name('api.payment.superpay.webhook');
});