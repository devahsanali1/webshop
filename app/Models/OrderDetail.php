<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $softDelete = true;
  
  	public function order(){
      return $this->belongsTo('App\Order', 'id');
    }

    public function product(){
      return $this->belongsTo('App\Product', 'id');
    }  
    
}

