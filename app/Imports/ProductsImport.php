<?php
namespace App\Imports;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Carbon\Carbon;
use App\Models\Product;

class ProductsImport implements ToModel, WithValidation, SkipsOnError, SkipsOnFailure, WithStartRow
{

    use Importable, SkipsErrors, SkipsFailures;

    private $rows = 0;
    
    /**
    * start row reading.
    *
    */
    public function startRow(): int
    {
        return 2;
    }
    /**
    * @param array $row
    *
    */
    public function model(array $row)
    {
        ++$this->rows;
        return Product::create([
            'title' => $row[1],
            'price' => $row[2],
        ]);
    }
    /**
    * rulest for importing data.
    *
    */
    public function rules(): array
    {
        return [
            '1' => ['unique:products,title', 'required'],
            '2' => 'required|numeric|min:1',
        ];
    }
    /**
    * getter for rows count.
    *
    */
    public function getRowCount(): int
    {
        return $this->rows;
    }   

}
