<?php
namespace App\Imports;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Carbon\Carbon;
use App\Models\Customer;

class CustomersImport implements ToModel, WithValidation, SkipsOnError, SkipsOnFailure
{

    use Importable, SkipsErrors, SkipsFailures;

    private $rows = 0;

    /**
    * @param array $row
    *
    */
    public function model(array $row)
    {
        ++$this->rows;
        return Customer::create([
            'first_name' => explode(" ",$row[3])[0],
            'last_name' => @explode(" ",$row[3])[1],
            'email' => $row[2],
            'phone' => $row[5],
            'job_titel' => $row[1],
            'created_at' => @Carbon::createFromFormat('l,F d,Y', $row[4])->toDateTimeString(),

        ]);
    }
    /**
    * rulest for importing data.
    *
    */
    public function rules(): array
    {
        return [
            '2' => ['unique:customers,email', 'required', 'regex:/(.+)@(.+)\.(.+)/i'],
            '3' => 'required|string',
            '5' => 'required|string',
            '4' => ['required', 'Date', 'regex:/(.+),(.+) (.+),(.+)/i'],
        ];
    }
    /**
    * getter for rows count.
    *
    */
    public function getRowCount(): int
    {
        return $this->rows;
    }   

}
