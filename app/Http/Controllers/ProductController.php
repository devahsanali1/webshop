<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	//
    }

    /**
     * Import product file.
     *
     */
    public function import()
    {
        if(!Storage::disk('local')->exists(env('PRODUCT_CSV'))){
            return response()->json([
                'status' => false,
                'message' => 'Import file not found.'
            ]);
        }
        $import = new ProductsImport();
        $import->import(env('PRODUCT_CSV'));
        $failed_count = sizeof($import->failures());
        $total_rows = $import->getRowCount() + $failed_count; 
        $data = [
            'total_rows' => $total_rows, 
            'total_succeed_rows' => $import->getRowCount(),
            'total_failed_rows' => $failed_count,
            'failed_data' => $import->failures(),
        ];
        Log::info($data);
        return response()->json($data);        
    }

}
