<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as Guzzle;
class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * request for order payment.
     *
     */
    public function payment(Request $request)
    {
        //validate required data
        $validator = \Validator::make($request->all(), [
            'transaction_id' => 'required',
            'marchant_id' => 'required|numeric', //to check payment service provider
            'card_no' => 'required|numeric|digits:14',
            'cvv_no' => 'required|numeric|digits:4',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }

        //validate order id
        $order = Order::with(['details', 'customer'])->where([
                    'id' => $request->order_id, 
                    'payed' => 0,
                    'transaction_id' => null
                ])->first();
        if(!$order){
            return response()->json([
                'status' => false,
                'message' => 'Invalid order id or order is payed'
            ]);
        }
        //check if superpay marchant
        if($request->marchant_id == 1){
            $data = $this->superpay_payment($request, $order);
        }

        if(isset($data) && $data == true){
           return response()->json([
                'status' => true,
                'message' => 'Payment Successful'
            ]); 
        }else{
           return response()->json([
                'status' => false,
                'message' => 'Transaction Failed'
            ]);  
        }
     
    }
    /**
     * send detials to the payment service provider.
     *
     */
    public function superpay_payment($request, $order)
    {
        //marchant url for payment
        $superpay = env('SUPERPAY');
        //curl request for payment
        $guzzle = new Guzzle(['verify' => false]);
        try {
             $response = $guzzle->post($superpay, [
                        'params' => [
                            'order_id' => $request->order_id,
                            'transaction_id' => $request->transaction_id,
                            'value' => $order->details->sum('price'),
                            'customer_email' => $order->customer->email,
                            'card_no' => $order->card_no,
                            'cvv_no'  => $order->cvv_no,
                        ],
                    ]);
             //check payemt has been made successfully
             if ($response->getStatusCode() == 200 && $response->getBody()->getContents() == 'Payment Successful') {
                //update transaction id
                Order::where('id', $request->order_id)->update([
                    'transaction_id' => $request->transaction_id
                ]);
                return true;
             }
        } catch (GuzzleException $exception) {
            //$exception->getCode();
            return false;
        }
       
        return false;
    }
    /**
     * registered webhook, service provider will hit this url with required
     * param for payment confirmation.
     *
     */
    public function superpay_webhook(Request $request){
        if(isset($request->transaction_id) && isset($request->order_id)){
            //validate ids
            $data = Order::where('id', $request->order_id)->where([
                        'transaction_id' => $request->transaction_id, 
                        'payed' => 0
                    ])->first();
            if($data){
                //mark order record as payed
                Order::where('id', $request->order_id)->update(['payed' => 1]);
                return true;
            }
        }
        return false;
    }
}
    