<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Get all orders or single order.
     *
     */
    public function index(Request $request)
    {
        if(isset($request->id) && $request->id){
           $data = Order::with(['details', 'customer'])->where('id', $request->id)->first();
           if(!$data){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid Order ID'
                ]);
           }
        }else{
           $data = Order::with(['details', 'customer'])->paginate(25);
        }
        return response()->json([
                    'status' => true,
                    'data' => $data,
                    'message' => 'Successfully fetched'
                ]);
    }
    /**
     * post orders entry.
     *
     */
    public function post(Request $request)
    {
        //validate required data
        $validator = \Validator::make($request->all(), [
            'customer_id' => 'required',
            'details' => "required|array|min:1",
            'details.*.product_id' => 'required|numeric',
            'details.*.price' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }
        //validate product ids
        $products_id = array_column($request->details, 'product_id');
        $find_prodocts = Product::whereIn('id', $products_id)->count();
        if(sizeof($products_id) != $find_prodocts){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Product IDs'
            ]);
        }

        //validate customer id
        $customer = Customer::where('id', $request->customer_id)->first();
        if(!$customer){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Customer ID'
            ]);
        }
        //if it is existing record
        if(isset($request->id) && $request->id){
           //validate order id
           $data = Order::where('id', $request->id)->first();
           if(!$data){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid Order ID'
                ]);
           }
           //update order data
           Order::where('id', $request->id)->update([
            'customer_id' => $request->customer_id,
           ]);
           //delete previous order details
           OrderDetail::where('order_id', $request->id)->forceDelete();
        }else{
           $data = new Order();
           $data->customer_id = $request->customer_id;
           $data->save();
        }
        //insert product details data
        $details = $request->details;
        for($i=0; $i<sizeof($details); $i++){
           $detail_data = new OrderDetail();
           $detail_data->order_id   = $data->id;
           $detail_data->product_id = $details[$i]['product_id'];
           $detail_data->price      = $details[$i]['price'];
           $detail_data->save();
        }

        return response()->json([
            'status' => true,
            'data' => ['id' => $data->id],
            'message' => 'Successfully posted'
        ]);
    }
    /**
     * Attach product with existing order.
     *
     */
    public function attach_product(Request $request)
    {
        //validate required data
        $validator = \Validator::make($request->all(), [
            'product_id' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }

        //validate product id
        $product = Product::where('id', $request->product_id)->first();
        if(!$product){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Product ID'
            ]);
        }

        //validate order id
        $order = Order::where('id', $request->id)->where('payed', 0)->first();
        if(!$order){
            return response()->json([
                'status' => false,
                'message' => 'Invalid order id or order is payed'
            ]);
        }

        //validate product id if already exist
        $data = OrderDetail::where([
                    'order_id' => $request->id, 
                    'product_id' => $request->product_id, 
                ])->first();
        if($data){
            return response()->json([
                'status' => false,
                'message' => 'Product already attached'
            ]);
        }
       
        //insert product details data
        $data = new OrderDetail();
        $data->order_id   = $order->id;
        $data->product_id = $product->id;
        $data->price      = $product->price;
        $data->save();

        return response()->json([
            'status' => true,
            'message' => 'Successfully added product'
        ]);
    }
    /**
     * Soft delete order.
     *
     */
    public function delete(Request $request){
        //validate order id
        $data = Order::where('id', $request->id)->first();
        if(!$data){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Order ID'
            ]);
        }
        //soft delete order record
        Order::where('id', $request->id)->delete();
        //soft delete order detail record
        OrderDetail::where('order_id', $request->id)->delete();
        return response()->json([
            'status' => true,
            'message' => 'Successfully deleted'
        ]);
    }
}
    